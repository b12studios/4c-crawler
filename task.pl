#!/usr/bin/perl

use strict;
use warnings;
use DBI;
use Data::Dumper;
use lib qw(/opt/4c-crawler/lib);
use search;
use Fcntl qw(LOCK_EX LOCK_NB);
use File::NFSLock;
use DateTime;
use Parallel::ForkManager;
use Scope::OnExit;

my $pm = Parallel::ForkManager->new(10);
my $date = DateTime->now;

my $DNS = "dbi:mysql:crawler:127.0.0.1:3306";
my $dbh = DBI->connect($DNS, 'root', '' ) or die $DBI::errstr;
my $comp;
my $run = $ARGV[0];
if ($run =~ /test/){
$comp = $dbh->selectall_hashref('select * from urls', 'id');
}
if ($run =~ /production/){
$comp = $dbh->selectall_hashref('select * from urls where lastRun < DATE_SUB(NOW(),INTERVAL rate MINUTE) ', 'id');
}
my $keywords = $dbh->selectall_arrayref('SELECT keyword FROM keywords');

my $regex = "(";
foreach (@$keywords) {
  $regex .= $_->[0] . "|";

}
$regex =~ s/\|$/)/;
$pm->run_on_finish(sub {
        my ($pid, $exit_code, $ident) = @_;
                #print "just got out of the pool with PID $pid and exit code: $exit_code \n";
                }
                );

$pm->run_on_start(
                sub { my ($pid,$ident)=@_;
                #print "started, pid: $pid \n";
                }
                );
$pm->run_on_wait(
                sub {
          #print "waiting on you \n";
                },
                0.5
                );
 my $search = search->new();
foreach my $key (keys %{$comp}){
    my $pid = $pm->start and next;
    my $DNS = "dbi:mysql:crawler:127.0.0.1:3306";
    my $dbh = DBI->connect($DNS, 'root', '' ) or die $DBI::errstr;
    on_scope_exit {
        $pm->finish; # Terminates the child process
    };
        my $domain = $comp->{$key}->{'url'};
        my $update = "UPDATE urls SET lastRun = NOW() WHERE id = $key";
        print $domain;
        $dbh->do($update);

        $search->main({ keyword   => $regex,
                        domain    => $domain,
                        searchId  => $key});
        $pm->finish;
}
$pm->wait_all_children;
