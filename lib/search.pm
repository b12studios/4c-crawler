package search;

use strict;
use warnings;
use WWW::Robot;
use LWP::UserAgent;
#use CGI::Pretty qw(-no_debug :html);
use HTML::Entities;
use DBI;
use DateTime;
use Data::Dumper;
use CGI::Carp qw(fatalsToBrowser);
use threads;
use threads::shared;
use Digest::MD5  qw(md5_hex);
use Parallel::ForkManager;

sub new{
    my $class = shift;
    my $self = { };
    bless $self, $class;
}
sub logit {
    print "\n"."[ ".time." ] ".$_[0];
}

sub main{
    my $self = shift;
    my $args = shift;
    our $domain = $args->{'domain'};
    my $keyword = $args->{'keyword'};
    our $searchId = $args->{'searchId'};
    my $date = DateTime->now;
    my %pages_w_keyword;
    my $contents;
    my $DNS = "dbi:mysql:crawler:127.0.0.1:3306";
    my $dbh = DBI->connect($DNS, 'root', '' ) or die $DBI::errstr;
    $|++;
    

    my @URL = "http://$domain";
    sub OK_TO_FOLLOW {
        my $uri = shift;              # URI object, known to be http onl

        for ($uri){
            return 0 unless m/$domain/i;
        }
        #for ($uri->query) {
        #  return 0 if defined $_ and length;
        #}
        for ($uri->path) {
            return 0 if /^\/(cgi|fors|-)/;
            #return 0 unless /(\.html?|\/)$/;
        }
        return 1;
    }
    ##end OK_TO_FOLLOW SUB
 my $robot = WWW::Robot->new
    (
        NAME => 'CleanOurSite',
        VERSION => '1.0',
        EMAIL => 'crappy@gmail.com',
        USERAGENT => LWP::UserAgent->new,
        #USERAGENT => 'Mozilla/5.0',
        CHECK_MIME_TYPES => 0,
        ##VERBOSE => 1,
        IGNORE_TEXT => 0
    );

#   $robot->env_proxy;

    $robot->addHook
    ("follow-url-test" => sub {
            my ($robot, $hook, $url) = @_;
            return 0 unless $url->scheme eq 'http';
            OK_TO_FOLLOW($url);
        });

    $robot->addHook
    ("invoke-on-contents" => sub {
            my ($robot, $hook, $url, $response, $structure) = @_;
            $contents = $response->content;
            if ($contents =~ m/$keyword/i) {
                  my $hit = $1;
                  my $clientId = $dbh->selectrow_arrayref("SELECT client_id from keywords where keyword = '$hit'");
                  my $keywordId = $dbh->selectrow_arrayref("SELECT id from keywords where keyword = '$hit'");
                  
                    if ($url =~ /thread/){
                        #logit($url);
                        #logit($hit);
                        my $result = $dbh->prepare("INSERT INTO results (url, keyword, urlMD5, urls_id, keywords_client_id, keywords_id) VALUES (?, ?, ?, ?, ?, ?)");
                        my $urlmd5 = md5_hex( $url );
                        $result->execute($url, $hit, $urlmd5, $searchId, $clientId->[0], $keywordId->[0]);
                    }
            }
        });
    my $test = $robot->run(@URL);
}
1;
