-- MySQL Script generated by MySQL Workbench
-- 08/16/15 03:24:16
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema crawler
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `crawler` ;

-- -----------------------------------------------------
-- Schema crawler
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `crawler` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `crawler` ;

-- -----------------------------------------------------
-- Table `crawler`.`client`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `crawler`.`client` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `name` VARCHAR(45) NOT NULL COMMENT '',
  `email` VARCHAR(45) NULL COMMENT '',
  `phone` VARCHAR(45) NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `crawler`.`keywords`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `crawler`.`keywords` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `keyword` VARCHAR(45) NOT NULL COMMENT '',
  `client_id` INT NOT NULL COMMENT '',
  PRIMARY KEY (`id`, `client_id`)  COMMENT '',
  INDEX `fk_keywords_client1_idx` (`client_id` ASC)  COMMENT '',
  CONSTRAINT `fk_keywords_client1`
    FOREIGN KEY (`client_id`)
    REFERENCES `crawler`.`client` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `crawler`.`urls`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `crawler`.`urls` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `url` VARCHAR(255) NOT NULL COMMENT '',
  `rate` INT NOT NULL COMMENT '',
  `lastRun` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `crawler`.`results`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `crawler`.`results` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `clientId` INT NOT NULL COMMENT '',
  `url` VARCHAR(255) NOT NULL COMMENT '',
  `urlMD5` VARCHAR(100) NOT NULL COMMENT '',
  `keywords_id` INT NOT NULL COMMENT '',
  `keywords_client_id` INT NOT NULL COMMENT '',
  `urls_id` INT NOT NULL COMMENT '',
  PRIMARY KEY (`id`, `keywords_id`, `keywords_client_id`, `urls_id`)  COMMENT '',
  INDEX `fk_results_keywords1_idx` (`keywords_id` ASC, `keywords_client_id` ASC)  COMMENT '',
  INDEX `fk_results_urls1_idx` (`urls_id` ASC)  COMMENT '',
  CONSTRAINT `fk_results_keywords1`
    FOREIGN KEY (`keywords_id` , `keywords_client_id`)
    REFERENCES `crawler`.`keywords` (`id` , `client_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_results_urls1`
    FOREIGN KEY (`urls_id`)
    REFERENCES `crawler`.`urls` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `crawler`.`images`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `crawler`.`images` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `localPath` VARCHAR(255) NOT NULL COMMENT '',
  `name` VARCHAR(255) NOT NULL COMMENT '',
  `hasBarcode` INT NULL COMMENT '',
  `checksum` VARCHAR(255) NOT NULL COMMENT '',
  `duplicate` INT NULL COMMENT '',
  `results_id` INT NOT NULL COMMENT '',
  PRIMARY KEY (`id`, `results_id`)  COMMENT '',
  INDEX `fk_images_results_idx` (`results_id` ASC)  COMMENT '',
  CONSTRAINT `fk_images_results`
    FOREIGN KEY (`results_id`)
    REFERENCES `crawler`.`results` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `crawler`.`downloads`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `crawler`.`downloads` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `localPath` VARCHAR(255) NOT NULL COMMENT '',
  `results_id` INT NOT NULL COMMENT '',
  `results_keywords_id` INT NOT NULL COMMENT '',
  `results_keywords_client_id` INT NOT NULL COMMENT '',
  `results_urls_id` INT NOT NULL COMMENT '',
  `done` INT NULL COMMENT '',
  PRIMARY KEY (`id`, `results_id`, `results_keywords_id`, `results_keywords_client_id`, `results_urls_id`)  COMMENT '',
  INDEX `fk_downloads_results1_idx` (`results_id` ASC, `results_keywords_id` ASC, `results_keywords_client_id` ASC, `results_urls_id` ASC)  COMMENT '',
  CONSTRAINT `fk_downloads_results1`
    FOREIGN KEY (`results_id` , `results_keywords_id` , `results_keywords_client_id` , `results_urls_id`)
    REFERENCES `crawler`.`results` (`id` , `keywords_id` , `keywords_client_id` , `urls_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
