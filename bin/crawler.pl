#!/usr/bin/perl -w

use strict;
use Proc::ProcessTable;
use Data::Dumper;

# become daemon
my $pid = fork();
print $pid,"\n";
if($pid) {
  #end parent process
  print "#parent process";
  exit(0);
}else {
  print "#child process";
}

# set new process group
setpgrp;
# setsid ? only C

LINE: while(1) {
  sleep(60);
  my $t = new Proc::ProcessTable;
  foreach my $p ( @{$t->table} ) {
       if ( $p->{cmndline} =~ /\/usr\/bin\/perl \/opt\/4c-crawler\/task.pl/ ){
        next LINE ;
     }

  }
  my $proccess = `/usr/bin/perl /opt/4c-crawler/task.pl production 1>2&`;
  open (FILE, ">>/var/log/crawler.log");
  print FILE ($proccess);
  close FILE;
  sleep(60);
}
