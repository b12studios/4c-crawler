#!/usr/bin/perl

use strict;
use warnings;
use lib qw(/opt/lib);
use Image::Thumbnail;
use Config2;

my $db = Config2->keyword;

my $path_raw = $db->selectall_arrayref("SELECT path, id FROM download WHERE done = 1 AND path IS NOT NULL", {Slice => {}});

foreach my $path (@$path_raw) {
        my $id = $path->{id};

        my $src_dir = "/mnt/HistoricalImages$path->{path}image";
        my $dest_dir = "/mnt/HistoricalImages$path->{path}thumb";
        `mkdir -p $dest_dir` unless -d $dest_dir;
        `mkdir -p $src_dir` unless -d $src_dir;
}

my $img_path = $db->selectall_arrayref("SELECT id, imagepath FROM imagemd5 WHERE thumb IS NULL", {Slice => {}});

foreach my $path (@$img_path){
        my $src_file = $path->{imagepath};
        my $target = $src_file ;
        $target =~ s/image/thumb/;
        if ($target =~ /je?pg$|png/){
        print "$src_file \n$target \n";
                my $t = new Image::Thumbnail(
                                module => "Image::Magick",
                                size => 100,
                                create => 1,
                                inputpath => "$src_file",
                                outputpath => "$target",
                                );
         my $set = $db->prepare("UPDATE imagemd5 SET thumb = ? WHERE imagepath = ? ");
         $set->execute($target, $src_file);
        }

}
