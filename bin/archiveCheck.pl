#!/usr/bin/perl

use strict;
use warnings;
use DBI;
use Fcntl qw(LOCK_EX LOCK_NB);
use LWP::UserAgent;
use Parallel::ForkManager;
use LWP::Simple;

my $pm = Parallel::ForkManager->new(10);

my $ua = LWP::UserAgent->new;
$ua->agent("Mozilla/8.0");

$pm->run_on_finish(
                sub { my ($pid, $exit_code, $ident) = @_;
                print "** $ident just got out of the pool with PID $pid and exit code: $exit_code\n" if defined($ident);
                }
                );

$pm->run_on_start(
                sub { my ($pid,$ident)=@_;
                print "** $ident started, pid: $pid\n" if defined($ident);
                }
                );
$pm->run_on_wait(
                sub {
                print "** Have to wait for one children ...\n"
                },
                0.5
                );

my $DNS = "dbi:mysql:crawler:localhost:3306";
my $dbh = DBI->connect($DNS, 'root', '' ) or die $DBI::errstr;

my $todo = $dbh->selectall_hashref("SELECT * FROM results where done = 0 OR done IS NULL", 'id');
foreach my $keys (keys %{$todo}){
    my $pid = $pm->start and next;
    my $url       = $todo->{$keys}->{'url'};
    my $urlId     = $todo->{$keys}->{'urls_id'};
    my $brandId   = $todo->{$keys}->{'keywords_client_id'};
    my $keywordId = $todo->{$keys}->{'keywords_id'};
    my $content = get($url);    
    my $DNS2 = "dbi:mysql:crawler:localhost:3306";
    my $dbh2 = DBI->connect($DNS2, 'root', '' ) or die $DBI::errstr;
    if(defined($content) && $content =~ m/Thread archived./){
         print "$url \n";
         $dbh2->do("UPDATE results SET done = 1 WHERE id = $keys");
    }
    $pm->finish;
}
$pm->wait_all_children;