#!/usr/bin/perl

use strict;
use warnings;
use DBI;
use Fcntl qw(LOCK_EX LOCK_NB);
use LWP::UserAgent;
use Parallel::ForkManager;

my $pm = Parallel::ForkManager->new(10);

my $ua = LWP::UserAgent->new;
$ua->agent("Mozilla/8.0");

$pm->run_on_finish(
                sub { my ($pid, $exit_code, $ident) = @_;
                print "** $ident just got out of the pool ".
                "with PID $pid and exit code: $exit_code\n";
                }
                );

$pm->run_on_start(
                sub { my ($pid,$ident)=@_;
                print "** $ident started, pid: $pid\n";
                }
                );
$pm->run_on_wait(
                sub {
                print "** Have to wait for one children ...\n"
                },
                0.5
                );

my $DNS = "dbi:mysql:crawler:localhost:3306";
my $dbh = DBI->connect($DNS, 'root', '' ) or die $DBI::errstr;

my $todo = $dbh->selectall_hashref("SELECT * FROM results where done = 0 OR done IS NULL", 'id');

foreach my $keys (keys %{$todo}){
        my $pid = $pm->start and next;

        my $url = $todo->{$keys}->{'url'};
        my $urlId = $todo->{$keys}->{'urls_id'};
        my $brandId = $todo->{$keys}->{'keywords_client_id'};
        my $keywordId = $todo->{$keys}->{'keywords_id'};
        my $path = "$brandId/$urlId/$keywordId/$keys/";
        my $down = "/mnt/HistoricalImages/new/$brandId/$urlId/$keywordId/$keys";
        my $images = "/mnt/HistoricalImages/new/$brandId/$urlId/$keywordId/$keys/images";
        my @array = split(/\//, $url);
        my $board = $array[3];
        my $thread = $array[5];
        print "$path \n";
        my $req = HTTP::Request->new(GET => "$url");
        my $res = $ua->request($req);
        if ($res->is_success) {
                my $stmnt = $dbh->prepare("UPDATE results SET localPath = ?, done = 0 WHERE id = ?");
                $stmnt->execute($path,$keys);
                `mkdir -p $images`;
                my $output = `wget -e robots=off -U "Mozilla/5.0 (compatible; Konqueror/3.2; Linux)" -N -E -H -k -K -p $url -P $down `;
                my $imageoutput = `/opt/4c-crawler/bin/imagedownloader.pl $thread -b $board -d $images`;
        } else {
                $dbh->do("UPDATE results SET localPath = '$path', done = 1 WHERE id = $keys");
        }

    $pm->finish;
}
$pm->wait_all_children;
