#!/usr/bin/perl

use strict;
use warnings;
use DBI;
use Digest::MD5 qw(md5 md5_hex md5_base64);
use Data::Dumper;
my $DNS = "dbi:mysql:Crawler:localhost:3306";
my $dbh = DBI->connect($DNS, 'root', '' ) or die $DBI::errstr;

my $pathsql = q{
    SELECT path, id
    FROM download
    WHERE imagedone = 0 AND done = 1
    };
my $prep = $dbh->selectall_hashref($pathsql, 'id' ); # {Slice => {}});
my @files;
foreach my $path (keys %$prep){
    print Dumper($prep->{$path}->{path});
    #print $prep->{$path};
    my $test = "/mnt/HistoricalImages/" . $prep->{$path}->{'path'} . "image";
    #print $test;
    @files = <$test/*>;

foreach my $file (@files){
    my $test = `md5sum $file`;
    print $test . "\n";
    my @md5sum = split(' ', $test);
    my $md5 = @md5sum->[0];
    #print Dumper($md5);
    my $insertsql = q{
        INSERT INTO imagemd5
        (imagepath, imagemd5, urlId)
        VALUES (?, ?, ?)
        };
        $dbh->do("UPDATE download SET imagedone = 1 where id = $path ");
    my $insert = $dbh->prepare($insertsql);
    $insert->execute($file, $md5, $path);
    #print md5($file) . "\n";
}
}
